import { AttributeValue} from "@aws-sdk/client-dynamodb";
import { EventBridgeClient, PutEventsCommand, PutEventsRequestEntry} from "@aws-sdk/client-eventbridge";
import { SQSEvent, SQSRecord } from "aws-lambda";
import { unmarshall } from "@aws-sdk/util-dynamodb";
import { updateRecord, compareObjects,  } from "./utils";
import { ValueObject } from "./model";
const destinationBusArn = process.env.DETINATION_BUS_ARN;
const tableName = process.env.TABLE_NAME;
const tablePk = process.env.TABLE_PK;

const eventBridgeClient = new EventBridgeClient({ region: process.env.REGION });

// Create the EventBridge event
const processRecord = async (record: SQSRecord): Promise<PutEventsRequestEntry> => {
  const eventBody = JSON.parse(record.body);
  let updatedAttributes: ValueObject = {};
  const attributes: ValueObject = eventBody.detail;
  try {
    const response = await updateRecord(attributes);
    console.log("Update Reponse " , JSON.stringify(response));
    const prevStateAttr = response.Attributes;
    updatedAttributes = compareObjects(
      unmarshall(prevStateAttr as Record<string, AttributeValue>),
      attributes
    );
    updatedAttributes[tablePk as string] = attributes[tablePk as string];
  } catch (error) {
    console.log(JSON.stringify(error));
    return null as unknown as PutEventsRequestEntry;
  }
  return {
    EventBusName: process.env.DETINATION_BUS_ARN as string,
    Source: process.env.SOURCE as string,
    DetailType: process.env.DETAIL_TYPE as string,
    Detail: JSON.stringify(updatedAttributes),
  };
};

export const handler = async (event: SQSEvent) => {
  try
  {
    const entries = event.Records.map(processRecord);
    const results = await Promise.all(entries);
    console.log("RESULTS", JSON.stringify(results));
    const putEventCommand = new PutEventsCommand({ Entries: results.filter((result) => result !== null)});
    const response = await eventBridgeClient.send(putEventCommand);
    const failedCount = response.FailedEntryCount;
   if (failedCount && failedCount > 0) {
      console.error(
        "Unable to send entries to Event bus. Failed Count : " + failedCount
      );
   }
  }
  catch(error)
  {
    console.error(JSON.stringify(error));
  }
};

