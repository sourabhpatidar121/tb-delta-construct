export type Primitive = string | number | boolean | null | undefined;
export interface ValueArray extends Array<Value> {}
export interface ValueObject { [key: string]: Value;}
export type Value = Primitive | ValueArray | ValueObject;
