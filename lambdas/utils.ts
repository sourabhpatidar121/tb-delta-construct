import { UpdateItemCommandOutput } from "@aws-sdk/client-dynamodb";
import {UpdateItemCommand,UpdateItemCommandInput,ReturnValue,DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { marshall } from "@aws-sdk/util-dynamodb";
import { ValueObject, ValueArray } from "./model";
const destinationBusArn = process.env.DETINATION_BUS_ARN;
const tableName = process.env.TABLE_NAME;
const tablePk = process.env.TABLE_PK;

const dynamodbClient = new DynamoDBClient({ region: process.env.REGION });
export const updateRecord = async (attributes: ValueObject): Promise<UpdateItemCommandOutput> => {
  const updateExpression = [];
  const expressionAttributeNames: Record<string, string> = {};
  const expressionAttributeValues: ValueObject = {};

  // Build update expression, excluding the primary key
  for (const [key, value] of Object.entries(attributes)) {
    if (key !== tablePk) {
      updateExpression.push(`#${key} = :${key}`);
      expressionAttributeNames[`#${key}`] = key;
      expressionAttributeValues[`:${key}`] = value;
    }
  }

  const params: UpdateItemCommandInput = {
    TableName: tableName,
    Key: marshall({ [tablePk as string]: attributes[tablePk as string] }),
    UpdateExpression: `SET ${updateExpression.join(", ")}`,
    ExpressionAttributeNames: expressionAttributeNames,
    ExpressionAttributeValues: marshall(expressionAttributeValues),
    ReturnValues: ReturnValue.ALL_OLD, // Return the previous values
  };
  console.log("UPDATE ITEM COMMAND",JSON.stringify(params));
  const updateItemCommant = new UpdateItemCommand(params);
  return await dynamodbClient.send(updateItemCommant);
};

export const compareObjects = (obj1: ValueObject, obj2: ValueObject): ValueObject => {
  const diff: ValueObject = {};

  // Iterate over keys of obj1
  for (const key in obj1) {
    if (obj1.hasOwnProperty(key)) {
      // Check if key exists in obj2
      if (obj2.hasOwnProperty(key)) {
        const value1 = obj1[key];
        const value2 = obj2[key];

        if (Array.isArray(value1) && Array.isArray(value2)) {
          // Compare arrays
          if (!arraysEqual(value1, value2)) {
            diff[key] = value2;
          }
        } else if (
          typeof value1 === "object" &&
          typeof value2 === "object" &&
          value1 !== null &&
          value2 !== null
        ) {
          // Recursively compare objects
          const nestedDiff = compareObjects(
            value1 as ValueObject,
            value2 as ValueObject
          );
          if (Object.keys(nestedDiff).length > 0) {
            diff[key] = value2;
          }
        } else if (value1 !== value2) {
          // Values are different
          diff[key] = value2;
        }
      }
    }
  }

  // Iterate over keys of obj2 to find extra attributes
  for (const key in obj2) {
    if (obj2.hasOwnProperty(key) && !obj1.hasOwnProperty(key)) {
      diff[key] = obj2[key];
    }
  }

  return diff;
};

export const arraysEqual = (arr1: ValueArray, arr2: ValueArray): boolean => {
  if (arr1.length !== arr2.length) {
    return false;
  }

  for (let i = 0; i < arr1.length; i++) {
    const value1 = arr1[i];
    const value2 = arr2[i];

    if (Array.isArray(value1) && Array.isArray(value2)) {
      if (!arraysEqual(value1, value2)) {
        return false;
      }
    } else if (
      typeof value1 === "object" &&
      typeof value2 === "object" &&
      value1 !== null &&
      value2 !== null
    ) {
      if (
        Object.keys(
          compareObjects(value1 as ValueObject, value2 as ValueObject)
        ).length > 0
      ) {
        return false;
      }
    } else if (value1 !== value2) {
      return false;
    }
  }

  return true;
};
