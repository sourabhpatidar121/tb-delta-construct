#!/usr/bin/env node
import "source-map-support/register";
import * as cdk from "aws-cdk-lib";
import { DeltaAttributeConstructProps, DeltaEventStack } from "../lib/delta-event-stack";
import * as dotenv from "dotenv";

const app = new cdk.App();
const props: DeltaAttributeConstructProps = {
  stackName: "tb-delta-attr-construct-test",
  env: {
    account: "730335190934",
    region: "us-east-1",
  },
  service: "tb-delta-attr-construct" ,
  stage: "sand",
  sourceBusArn: "arn:aws:events:us-east-1:730335190934:event-bus/default",
  eventDetailType: "testDeltaEventProxy",
  eventSource: "testDeltaProxy",
  deltaTablePK: "external_id"
}
dotenv.config({ path: "env/.env.sand" });
new DeltaEventStack(app, "DeltaEventsStack", props);
