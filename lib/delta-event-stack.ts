import * as cdk from "aws-cdk-lib";
import { Construct } from "constructs";
import * as dynamodb from "aws-cdk-lib/aws-dynamodb";
import * as event_pipes from "aws-cdk-lib/aws-pipes";
import * as sqs from "aws-cdk-lib/aws-sqs";
import * as iam from "aws-cdk-lib/aws-iam";
import * as lambda from "aws-cdk-lib/aws-lambda";
import * as events from "aws-cdk-lib/aws-events";
import * as logs from 'aws-cdk-lib/aws-logs'
import * as lambda_eventsources from "aws-cdk-lib/aws-lambda-event-sources";
import * as target from 'aws-cdk-lib/aws-events-targets'
export interface DeltaAttributeConstructProps extends cdk.StackProps {
  service: string;
  stage: string;
  env: cdk.Environment;
  sourceBusArn: string;
  eventDetailType: string;
  eventSource: string;
  deltaTablePK: string;
}
export class DeltaEventStack extends cdk.Stack {
  constructor(
    scope: Construct,
    id: string,
    props: DeltaAttributeConstructProps
  ) {
    super(scope, id, props);
    const { eventDetailType, eventSource, stage, deltaTablePK, env, service, sourceBusArn } = props;
    
    // Destination Event Bus
    const deltaConstructDestinationBus = new events.EventBus(this, `${service}-${stage}-event-bus`,{
      eventBusName: `${service}-${stage}-event-bus`
    })

    const mockDevTestRule = new events.Rule(this, `${service}-${stage}-event-developer-observability-rule`, {
      eventBus: deltaConstructDestinationBus,
      ruleName: `${service}-${stage}-event-developer-observability-rule`,
      eventPattern: {
        source: [eventSource],
        detailType: [{
          prefix: eventDetailType
        } as unknown as string]
      },
      description: "Rule for developer observability"
    });
    const mockLogGroup = new logs.LogGroup(this, `${service}-${stage}-mock-bi-events-log`, {
      logGroupName: `${service}-${stage}-mock-log`
    });
    mockDevTestRule.addTarget(new target.CloudWatchLogGroup(mockLogGroup));

    // DynamoDB Table
    const attributeStateTable = new dynamodb.Table(this, `${service}-${stage}-${eventSource}`,{
        tableName:`${service}-${stage}-${eventSource}`,
        partitionKey: {
          name: deltaTablePK,
          type: dynamodb.AttributeType.STRING,
        }
    });
 
    // Buffer Queue
    const deltaEventQueuePending = new sqs.Queue(this,`${service}-${stage}-queue-pending`,{
      queueName: `${service}-${stage}-queue-pending`
    })
    // Buffer Queue
    const deltaEventQueueFailure = new sqs.Queue(this,`${service}-${stage}-buffer-queue-failure`,{
      queueName: `${service}-${stage}-queue-failure`
    })

    const sourceBus = events.EventBus.fromEventBusArn(this,`${service}-${stage}-source`, sourceBusArn)
    const deltaConstructEventFilterRule = new events.Rule(this, `${service}-${stage}-event-Rule`, {
      eventBus: sourceBus,
      ruleName: `${service}-${stage}-event-Rule`,
      eventPattern: {
        source: [eventSource],
        detailType: [{
          prefix: eventDetailType
        } as unknown as string]
      },
    });

    deltaConstructEventFilterRule.addTarget(
      new target.SqsQueue(deltaEventQueuePending, {
        deadLetterQueue: deltaEventQueueFailure,
        retryAttempts:2
      })
    )
    
    const processDeltaAttributesRole = new iam.Role(this, `${service}-${stage}-${eventSource}-role`,{
      roleName: `${service}-${stage}-${eventSource}-role`,
      assumedBy: new iam.ServicePrincipal('lambda.amazonaws.com'),
    })
    processDeltaAttributesRole.addManagedPolicy(iam.ManagedPolicy.fromAwsManagedPolicyName("service-role/AWSLambdaBasicExecutionRole"));
    processDeltaAttributesRole.addToPolicy( new iam.PolicyStatement({
      actions: ["dynamodb:BatchGetItem",
                "dynamodb:BatchWriteItem",
                "dynamodb:ConditionCheckItem",
                "dynamodb:PutItem",
                "dynamodb:DescribeTable",
                "dynamodb:DeleteItem",
                "dynamodb:GetItem",
                "dynamodb:Scan",
                "dynamodb:Query",
                "dynamodb:UpdateItem"
              ],
      effect: iam.Effect.ALLOW,
      resources: [attributeStateTable.tableArn]
    }))
    processDeltaAttributesRole.addToPolicy( new iam.PolicyStatement({
      actions: ["sqs:*"],
      effect: iam.Effect.ALLOW,
      resources: [deltaEventQueuePending.queueArn, deltaEventQueueFailure.queueArn]
    }))

    processDeltaAttributesRole.addToPolicy(new iam.PolicyStatement({
      effect: iam.Effect.ALLOW,
      actions: ["events:PutEvents"],
      resources: [deltaConstructDestinationBus.eventBusArn]
    }))

    const processDeltaAttributesFunction = new lambda.Function(this, `${service}-${stage}-${eventSource}-lam`, {
      functionName: `${service}-${stage}-${eventSource}-lam`,
      runtime: lambda.Runtime.NODEJS_20_X,
      handler: "processDetlaAttributes.handler",
      environment:{
        DETINATION_BUS_ARN: deltaConstructDestinationBus.eventBusArn,
        TABLE_NAME:attributeStateTable.tableName,
        TABLE_PK: deltaTablePK,
        REGION: env.region as string,
        DLQ_URL: deltaEventQueueFailure.queueUrl,
        SOURCE: eventSource,
        DETAIL_TYPE: eventDetailType 
      },
      code: lambda.Code.fromAsset("lambdas"),
      role: processDeltaAttributesRole
    });

    // Grant permissions to lambda to access the DynamoDB table
    processDeltaAttributesFunction.addEventSource(new lambda_eventsources.SqsEventSource(deltaEventQueuePending,{
      batchSize:10,
      maxBatchingWindow: cdk.Duration.seconds(1)
    }))


    // Output DynamoDB Table Name
    new cdk.CfnOutput(this, "TableName", {
      value: attributeStateTable.tableName,
    });
  }
}
